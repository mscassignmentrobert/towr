# TOWR+

TOWR+ is an application for phase-based gait optimisation, through whole-body dynamics.

Current version will not include base motion, only the end-effector and joint positions + torques.
For now joint motion and torque will not be phase based.

Note: Renamed this from Gambol to TOWR+. This project is merely an extension of TOWR itself. Gambol is now a seperate project.