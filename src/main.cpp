#include <cmath>
#include <iostream>
#include <unistd.h>

#include <towr/terrain/examples/height_map_examples.h>
#include <ifopt/ipopt_solver.h>

#include "NlpFormulation/NlpFormulation.h"
#include "Variables/MySplineHolder.h"
#include "Robots/MonopedMuJoCoModel.h"
#include "MuJoCoTools/MuJoCoViewer.hpp"
#include "MuJoCoTools/MuJoCoFigure.hpp"

using namespace towr;

//--------------------
#define TEST_NLP false
//--------------------

void splineHolderToMatlab(const MySplineHolder& solution, double dt);
void showMuJoCoSolution(const MySplineHolder& solution,
		const NlpFormulation& formulation);

//Eigen::VectorXd getStationaryTorque(const NlpFormulation& formulation);

// A minimal example how to build a trajectory optimization problem using TOWR.
//
// The more advanced example that includes ROS integration, GUI, rviz
// visualization and plotting can be found here:
// towr_ros/src/towr_ros_app.cc
int main()
{
	NlpFormulation formulation;

	// terrain
	formulation.terrain_ = std::make_shared<FlatGround>(0.0);

	// Set number of input torques
	int num_q_ = 3;
	formulation.params_.num_q_ = num_q_; // q1, q2, q3
	formulation.params_.num_torques_ = 2; // tau1, tau2

	formulation.wbd_model_ = std::make_shared<MonopedMuJoCoModel>(); // MuJoCo

	// Set initial and final positions
	formulation.initial_joint_pos_ = Eigen::VectorXd(num_q_);
	formulation.initial_joint_pos_ << -0.4, 1.0, -0.4;

	formulation.final_joint_pos_ = Eigen::VectorXd(num_q_);
	formulation.final_joint_pos_ << -0.5, 1.0, -0.4;

	// Which of the final joints to constrain
	formulation.params_.bounds_initial_joint_pos_ = { 0 };
	formulation.params_.bounds_final_joint_pos_ = { 0 };

	double distance = 0.5;
	formulation.initial_ee_W_.push_back(Eigen::Vector3d::Zero());
	formulation.final_ee_W_.push_back(Eigen::Vector3d(distance, 0.0, 0.0));

	// Joint limits
	formulation.joint_bounds_ = { 0, 1, 2 }; // All joints
	formulation.joint_limits_ = { std::make_pair(-M_PI_2, M_PI_4),
			std::make_pair(0.0, M_PI_2), std::make_pair(-M_PI_2, M_PI_2) };

	formulation.torque_bounds_ = { 0, 1 }; // All torques
	double torque_lim = 200.0;
	formulation.torque_limits_ = { std::make_pair(-torque_lim, torque_lim),
			std::make_pair(-torque_lim, torque_lim) };

	// Parameters that define the motion. See c'tor for default values or
	// other values that can be modified.
	// First we define the initial phase durations, that can however be changed
	// by the optimizer. The number of swing and stance phases however is fixed.
	// alternating stance and swing:     ____-----_____-----_____-----_____
	//formulation.params_.ee_phase_durations_.push_back( { 0.4, 0.2, 0.4, 0.2, 0.4, 0.2, 0.2 });

	// Start with 5 phases: (1.4 seconds total)
	//formulation.params_.ee_phase_durations_.push_back({ 0.4, 0.2, 0.4, 0.2, 0.2 });
	formulation.params_.ee_phase_durations_.push_back({ 0.4, 0.2, 0.2 });
	//formulation.params_.ee_phase_durations_.push_back({ 1.0 });
	formulation.params_.ee_in_contact_at_start_.push_back(true);
	int n_steps = (formulation.params_.ee_phase_durations_[0].size() - 1) / 2;
	double time_total = formulation.params_.GetTotalTime();
	double step_distance = distance / n_steps;

	// Joints initial guess
	auto j_guess = &formulation.joints_guess_;
	j_guess->push_back(std::make_pair(0.0, formulation.initial_joint_pos_));

	Eigen::VectorXd curled = Eigen::Vector3d(-0.2, 0.5, -0.1);
	Eigen::VectorXd stretched = Eigen::Vector3d(0.3, -0.6, 0.3);

	for (double step = 0.0; step < (double) n_steps; step += 1.0)
	{
		j_guess->push_back(
				std::make_pair(0.35 + 0.6 * step,
						formulation.initial_joint_pos_ + curled));
		j_guess->push_back(
				std::make_pair(0.4 + 0.6 * step,
						formulation.initial_joint_pos_ + stretched));
		j_guess->push_back(
				std::make_pair(0.6 + 0.6 * step,
						formulation.initial_joint_pos_ + stretched));
	}

	j_guess->push_back(
			std::make_pair(time_total, formulation.final_joint_pos_));

	// end-effector initial guess
	auto ee_guess = &formulation.ee_pos_guess_;

	double x = 0.0;
	for (int step = 0; step < n_steps; step++)
	{
		ee_guess->push_back(
				std::make_pair(0.0 + 0.6 * step, Eigen::Vector3d(x, 0.0, 0.0)));
		ee_guess->push_back(
				std::make_pair(0.2 + 0.6 * step, Eigen::Vector3d(x, 0.0, 0.0)));
		x += 0.5 * step_distance;
		ee_guess->push_back(
				std::make_pair(0.4 + 0.6 * step, Eigen::Vector3d(x, 0.0, 0.2)));
		x += 0.5 * step_distance;
	}
	ee_guess->push_back(
			std::make_pair(time_total - 0.2, Eigen::Vector3d(x, 0.0, 0.0)));
	ee_guess->push_back(
			std::make_pair(time_total, Eigen::Vector3d(x, 0.0, 0.0)));

	// Torque guess
	//formulation.torque_guess_ = getStationaryTorque(formulation);
	formulation.torque_guess_ = Eigen::Vector2d(0.0, 0.0);

	// Add cost
	formulation.params_.costs_.push_back({ Parameters::TorqueCostID, 1.0 });
	//formulation.params_.costs_.push_back({ Parameters::ForcesCostID, 1.0 });
	//formulation.params_.costs_.push_back({ Parameters::JointVelocityID, 1.0 });

	// Initialize the nonlinear-programming problem with the variables,
	// constraints and costs.
	ifopt::Problem nlp;
	MySplineHolder solution;

	for (auto c : formulation.GetVariableSets(solution))
		nlp.AddVariableSet(c);
	for (auto c : formulation.GetConstraints(solution))
		nlp.AddConstraintSet(c);
	for (auto c : formulation.GetCosts())
		nlp.AddCostSet(c);

	// Help joint motion a little bit
	//setJointInitialGuess(variables, solution, formulation.params_);

	// Choose ifopt solver (IPOPT or SNOPT), set some parameters and solve.
	// solver->SetOption("derivative_test", "first-order");
	auto solver = std::make_shared<ifopt::IpoptSolver>();
	solver->SetOption("linear_solver", "ma27");
	solver->SetOption("jacobian_approximation", "exact"); // exact / finite-difference-values
	solver->SetOption("print_level", 5);
	solver->SetOption("max_cpu_time", 20.0);
	solver->SetOption("max_iter", 300);

#if TEST_NLP
	solver->SetOption("derivative_test", "first-order");
	solver->SetOption("point_perturbation_radius", 0.3);
	solver->SetOption("derivative_test_perturbation", 1e-7);
	solver->SetOption("max_iter", 0);
#endif

	solver->Solve(nlp);

	// Can directly view the optimization variables through:
	// Eigen::VectorXd x = nlp.GetVariableValues()
	// However, it's more convenient to access the splines constructed from these
	// variables and query their values at specific times:
	using namespace std;
	cout.precision(5);
	nlp.PrintCurrent(); // view variable-set, constraint violations, indices,...

	if (solver->GetReturnStatus() == 0)
	{
		cout << endl << "---- Solution found! ----" << endl << endl;
	}
	else
	{
		cout << endl << "No proper solution found..." << endl << endl;
	}

	showMuJoCoSolution(solution, formulation);
	//splineHolderToMatlab(solution, 0.01);

	return solver->GetReturnStatus();
}

/**
 * Turn splineholder into MATLAB matrix
 *
 * Columns: time, base lineaer, base angualer, end-effectos positions, forces
 *
 * @param SplineHolder
 * @param double dt
 */
void splineHolderToMatlab(const MySplineHolder& solution, double dt)
{
	using namespace std;
	cout.precision(5);

	double t = 0.0;

	cout << "dt = " << dt << ';' << endl;
	cout << "N_ee = " << solution.phase_durations_.size() << ';' << endl;
	cout << "N_q = " << solution.joint_motion_->GetPoint(0).p().rows() << ';'
			<< endl;
	cout << "N_u = " << solution.joint_torque_->GetPoint(0).p().rows() << ';'
			<< endl;
	cout << "base = false;" << endl;

	cout << "data = [" << endl;

	while (t <= solution.joint_motion_->GetTotalTime() + 1e-5)
	{
		cout << t << " ";

		for (auto spline : solution.ee_motion_)
		{
			cout << spline->GetPoint(t).p().transpose() << ' ';
		}

		for (auto spline : solution.ee_force_)
		{
			cout << spline->GetPoint(t).p().transpose() << ' ';
		}

		cout << solution.joint_motion_->GetPoint(t).p().transpose() << ' ';

		cout << solution.joint_torque_->GetPoint(t).p().transpose() << ' ';

		cout << ";"; //cout << endl;

		t += dt;
	}

	cout << endl << "];" << endl;
}

/**
 * Play solution in MuJoCo
 */
void showMuJoCoSolution(const MySplineHolder& solution,
		const NlpFormulation& formulation)
{
	auto wbd_model = std::dynamic_pointer_cast<MonopedMuJoCoModel>(
			formulation.wbd_model_);
	const mjModel* m = wbd_model->GetMuJoCoModel().getModel();
	mjData* d = wbd_model->GetMuJoCoModel().getData();

	MuJoCoViewer viewer(m);

	// Move camera
	viewer.cam.distance = 5.0;

	bool done = false;

	double opt_time = solution.joint_motion_->GetTotalTime();

	MuJoCoFigure fig_torque("Torques", 2);
	MuJoCoFigure fig_force("Forces", 2);
	MuJoCoFigure fig_joints("Joints", 3);
	MuJoCoFigure fig_dynamics("Dynamics Error", 5);
	int w = 400, h = 200;
	fig_torque.setViewport(0, 0 * h, w, h);
	fig_force.setViewport(0, 1 * h, w, h);
	fig_joints.setViewport(0, 2 * h, w, h).setLegends({"q1", "q2", "q3"});
	fig_dynamics.setViewport(0, 3 * h, w, h);
	viewer.addFigure(&fig_torque);
	viewer.addFigure(&fig_force);
	viewer.addFigure(&fig_joints);
	viewer.addFigure(&fig_dynamics);

	// As window is still open
	while (!done)
	{
		double simstart = glfwGetTime();
		double video_time = 0.0;
		mj_resetData(m, d); // Reset simulation

		// Keep looping over solution
		while (video_time <= opt_time + 1.0) // Include a little pause
		{
			double tmstart = glfwGetTime();

			// Progress animation
			video_time = tmstart - simstart;

			if (video_time < opt_time - 1e-5)
			{
				wbd_model->SetCurrent(solution, video_time);
			}

			// Quit
			if (viewer.windowShouldClose())
			{
				done = true;
				break;
			}

			Eigen::VectorXd ddq_error(5);
			mju_copy(ddq_error.data(), d->qacc, 5);
			ddq_error -= wbd_model->GetCurrentDynamics();
			//ddq_error = wbd_model->GetCurrentDynamics();

			fig_torque.addValues(tmstart, &d->ctrl[0], 2);
			fig_force.addValues(tmstart, &d->ctrl[2], 2);
			fig_joints.addValues(tmstart, &d->qpos[2], 3);
			fig_dynamics.addValues(tmstart, &(ddq_error.data()[0]), 5);
			//fig_dynamics.addValue(tmstart, ddq_error[4]);

			viewer.update(d); // Render

			// Wait until a total of 1 frame time has passed
			while (glfwGetTime() - tmstart < 1.0 / 60.0)
			{
				usleep(500); // Sleep for 0.5 millisecond to prevent a busy wait
			}
		}
	}
}

/**
 * Get stationary torque during rest
 */
/*Eigen::VectorXd getStationaryTorque(const NlpFormulation& formulation)
 {
 auto wbd_model = std::dynamic_pointer_cast<MonopedMuJoCoModel>(
 formulation.wbd_model_);

 auto model = wbd_model->GetMuJoCoModel();

 Eigen::VectorXd q = Eigen::VectorXd::Zero(5);
 Eigen::VectorXd dq = Eigen::VectorXd::Zero(5);
 Eigen::VectorXd ddq = Eigen::VectorXd::Zero(5);

 q.segment(2, 3) = formulation.initial_joint_pos_;

 model.reset_data(q.data(), dq.data(), ddq.data());

 Eigen::VectorXd q_force(5);

 model.get_inverse_dynamics(ddq.data(), q_force.data());

 Eigen::MatrixXd B(5, 2);
 B << 0, 0, 0, 0, -1.0, 0, 1.0, -1.0, 0, 1.0;

 return B.colPivHouseholderQr().solve(q_force);
 }*/
