#include "Parameters.h"
#include <towr/variables/cartesian_dimensions.h>

#include <algorithm>
#include <numeric>      // std::accumulate
#include <math.h>       // fabs
#include <cassert>

namespace towr
{

Parameters::Parameters()
{
	// constructs optimization variables
	duration_joints_polynomial_ = 0.1;
	duration_torques_polynomial_ = duration_joints_polynomial_;
	force_polynomials_per_stance_phase_ = 3;
	ee_polynomials_per_swing_phase_ = 2; // so step can at least lift leg (default: 2)

	// parameters related to specific constraints (only used when it is added as well)
	force_limit_in_normal_direction_ = 10000;
	dt_constraint_dynamic_ = 0.5 * duration_joints_polynomial_;
	bound_phase_duration_ = std::make_pair(0.2, 1.0); // used only when optimizing phase durations, so gait

	// a minimal set of basic constraints
	constraints_.push_back(Terrain);
	//constraints_.push_back(Swing);
	constraints_.push_back(Force); // ensures unilateral forces and inside the friction cone.
	constraints_.push_back(DynamicTorque); // Constraint for joint torques
	//constraints_.push_back(JointAcc); // Smooth joint acceleration

	// optional costs to e.g penalize endeffector forces
	// costs_.push_back({ForcesCostID, 1.0}); weighed by 1.0 relative to other costs

	// bounds on final joints state
	//bounds_final_joint_pos_ = { }; // Set in main.cpp
	//bounds_final_joint_vel_ = { };

	// additional restrictions are set directly on the variables in nlp_factory,
	// such as e.g. initial and endeffector,...

	num_torques_ = 0;
	num_q_ = 0;
}

void Parameters::OptimizePhaseDurations()
{
	constraints_.push_back(TotalTime);
}

Parameters::VecTimes Parameters::GetPolyDurations(double dt) const
{
	std::vector<double> spline_timings_;
	double t_left = GetTotalTime();

	double eps = 1e-5; // since repeated subtraction causes inaccuracies
	while (t_left > eps)
	{
		double duration = t_left > dt ? dt : t_left;
		spline_timings_.push_back(duration);

		t_left -= dt;
	}

	return spline_timings_;
}

Parameters::VecTimes Parameters::GetJointsPolyDurations() const
{
	return GetPolyDurations(duration_joints_polynomial_);
}

Parameters::VecTimes Parameters::GetTorquesPolyDurations() const
{
	return GetPolyDurations(duration_torques_polynomial_);
}

int Parameters::GetPhaseCount(EEID ee) const
{
	return ee_phase_durations_.at(ee).size();
}

int Parameters::GetEECount() const
{
	return ee_in_contact_at_start_.size();
}

double Parameters::GetTotalTime() const
{
	std::vector<double> T_feet;

	for (const auto& v : ee_phase_durations_)
		T_feet.push_back(std::accumulate(v.begin(), v.end(), 0.0));

	// safety check that all feet durations sum to same value
	double T = T_feet.empty() ? 0.0 : T_feet.front(); // take first foot as reference
	for (double Tf : T_feet)
		assert(fabs(Tf - T) < 1e-6);

	return T;
}

bool Parameters::IsOptimizeTimings() const
{
	// if total time is constrained, then timings are optimized
	ConstraintName c = TotalTime;
	auto v = constraints_; // shorthand
	return std::find(v.begin(), v.end(), c) != v.end();
}

} // namespace towr
