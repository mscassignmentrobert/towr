#ifndef MY_NLP_FORMULATION_H
#define MY_NLP_FORMULATION_H

#include "NlpFormulation.h"

#include <towr/variables/spline_holder.h>
#include <towr/variables/variable_names.h>
#include <towr/variables/phase_durations.h>
#include "../Variables/MySplineHolder.h"
#include "../Variables/MyVariableNames.h"

#include <towr/constraints/base_motion_constraint.h>
#include <towr/constraints/force_constraint.h>
#include <towr/constraints/range_of_motion_constraint.h>
#include <towr/constraints/swing_constraint.h>
#include <towr/constraints/terrain_constraint.h>
#include <towr/constraints/total_duration_constraint.h>
#include <towr/constraints/spline_acc_constraint.h>
#include <towr/constraints/dynamic_constraint.h>
#include "../Constraints/DynamicTorqueConstraint.h"
#include <towr/costs/node_cost.h>
#include <towr/variables/nodes_variables_all.h>
#include <iostream>
#include "../Variables/MyNodesVariablesAll.h"
#include "../Variables/MyNodesVariablesEEMotion.h"

namespace towr
{

NlpFormulation::NlpFormulation()
{
	using namespace std;
	cout << "\n";
	cout << "************************************************************\n";
	cout << " TOWR+ - Trajectory Optimization for Walking Robots (v0.1)\n";
	cout << "   \u00a9 Alexander W. Winkler, modified by Robert A. Roos\n";
	cout << "           https://github.com/ethz-adrl/towr\n";
	cout << "************************************************************";
	cout << "\n\n";
}

/**
 * Get variable sets and update spline_holder
 *
 * The variable sets are:
 *
 *  - Base motion (linear)		[default]
 *  - Base motion (angular)		[default]
 *  - Joint motion (joint coordinates)
 *  - Joint torques
 *  	foreach end-effector:
 *  		- end-effector position 	[default]
 *  		- end-effector force		[default]
 *  		- contact shecules			[default]
 */
NlpFormulation::VariablePtrVec NlpFormulation::GetVariableSets(
		MySplineHolder& spline_holder)
{
	VariablePtrVec vars;

	// Each Make...() method returns a list of variable sets

	auto joint_motion = MakeJointVariables();
	vars.push_back(joint_motion);

	auto joint_torque = MakeTorqueVariables();
	vars.push_back(joint_torque);

	auto ee_motion = MakeEndeffectorVariables();
	vars.insert(vars.end(), ee_motion.begin(), ee_motion.end());

	auto ee_force = MakeForceVariables();
	vars.insert(vars.end(), ee_force.begin(), ee_force.end());

	auto contact_schedule = MakeContactScheduleVariables();
	// can also just be fixed timings that aren't optimized over, but still added
	// to spline_holder.
	if (params_.IsOptimizeTimings())
	{
		vars.insert(vars.end(), contact_schedule.begin(),
				contact_schedule.end());
	}

	// stores these readily constructed spline
	spline_holder = MySplineHolder(joint_motion,
			params_.GetJointsPolyDurations(), joint_torque,
			params_.GetTorquesPolyDurations(), ee_motion, ee_force,
			contact_schedule, params_.IsOptimizeTimings());

	return vars;
}

/**
 * Return joint motion variables
 */
NodesVariables::Ptr NlpFormulation::MakeJointVariables() const
{
	int n_nodes = params_.GetJointsPolyDurations().size() + 1;

	// Create splines
	auto spline_joints = std::make_shared<MyNodesVariablesAll>(n_nodes,
			params_.num_q_, id::joint_motion_nodes); // One joint per dimension

	// Add joint limits
	Eigen::VectorXd lim_l(params_.num_q_);
	Eigen::VectorXd lim_u(params_.num_q_);
	std::vector<int> dims;
	for (int i = 0; i < params_.num_q_; i++)
	{
		lim_l[i] = joint_limits_[i].first;
		lim_u[i] = joint_limits_[i].second;
	}
	spline_joints->AddGlobalBounds(kPos, joint_bounds_, lim_l, lim_u);

	// Set initial guess to bended knee
	if (joints_guess_.empty())
	{
		spline_joints->SetByLinearInterpolation(initial_joint_pos_,
				final_joint_pos_, params_.GetTotalTime());
	}
	else
	{
		spline_joints->SetByTimeInterpolation(joints_guess_);
	}

	spline_joints->AddStartBound(kPos, params_.bounds_initial_joint_pos_,
			initial_joint_pos_);
	spline_joints->AddFinalBound(kPos, params_.bounds_final_joint_pos_,
			final_joint_pos_);

	return spline_joints;
}

/**
 * Return joint torque variables
 */
NodesVariables::Ptr NlpFormulation::MakeTorqueVariables() const
{
	int n_nodes = params_.GetTorquesPolyDurations().size() + 1;

	// Create splines
	auto spline_torque = std::make_shared<MyNodesVariablesAll>(n_nodes,
			params_.num_torques_, id::joint_torque_nodes); // One joint per dimension

	// Add torque limits
	Eigen::VectorXd lim_l(params_.num_torques_);
	Eigen::VectorXd lim_u(params_.num_torques_);
	std::vector<int> dims;
	for (int i = 0; i < params_.num_torques_; i++)
	{
		lim_l[i] = torque_limits_[i].first;
		lim_u[i] = torque_limits_[i].second;
	}
	spline_torque->AddGlobalBounds(kPos, torque_bounds_, lim_l, lim_u);

	// Set initial guess through interpolation (set to zero)
	Eigen::VectorXd v(params_.num_torques_);
	if (torque_guess_.size() == 0)
	{
		v.setZero();
	}
	else
	{
		v = torque_guess_;
	}
	spline_torque->SetByLinearInterpolation(v, v, params_.GetTotalTime());

	return spline_torque;
}

/**
 * Return end-effector variables (positions only)
 */
std::vector<NodesVariablesPhaseBased::Ptr> NlpFormulation::MakeEndeffectorVariables() const
{
	std::vector<NodesVariablesPhaseBased::Ptr> vars;

	// Endeffector Motions
	double T = params_.GetTotalTime();
	for (int ee = 0; ee < params_.GetEECount(); ee++)
	{
		auto nodes = std::make_shared<MyNodesVariablesEEMotion>(
				params_.GetPhaseCount(ee),
				params_.ee_in_contact_at_start_.at(ee), id::EEMotionNodes(ee),
				params_.ee_polynomials_per_swing_phase_);

		// initialize towards final footholds
		if (ee_pos_guess_.empty())
		{
			nodes->SetByLinearInterpolation(initial_ee_W_.at(ee),
					final_ee_W_.at(ee), T);
		}
		else
		{
			nodes->SetByTimeInterpolation(ee_pos_guess_);
		}

		nodes->AddStartBound(kPos, { X, Y, Z }, initial_ee_W_.at(ee));
		nodes->AddFinalBound(kPos, { X, Y, Z }, final_ee_W_.at(ee));
		vars.push_back(nodes);
	}

	return vars;
}

/**
 * Return end-effector force variables
 */
std::vector<NodesVariablesPhaseBased::Ptr> NlpFormulation::MakeForceVariables() const
{
	std::vector<NodesVariablesPhaseBased::Ptr> vars;

	double T = params_.GetTotalTime();
	for (int ee = 0; ee < params_.GetEECount(); ee++)
	{
		auto nodes = std::make_shared<NodesVariablesEEForce>(
				params_.GetPhaseCount(ee),
				params_.ee_in_contact_at_start_.at(ee), id::EEForceNodes(ee),
				params_.force_polynomials_per_stance_phase_);

		// initialize with mass of robot distributed equally on all legs
		double m = wbd_model_->GetTotalMass();
		double g = wbd_model_->GetG();

		Vector3d f_stance(0.0, 0.0, m * g / params_.GetEECount());
		nodes->SetByLinearInterpolation(f_stance, f_stance, T); // stay constant
		vars.push_back(nodes);
	}

	return vars;
}

/**
 * Return contact schedule variables
 */
std::vector<PhaseDurations::Ptr> NlpFormulation::MakeContactScheduleVariables() const
{
	std::vector<PhaseDurations::Ptr> vars;

	for (int ee = 0; ee < params_.GetEECount(); ee++)
	{
		auto var = std::make_shared<PhaseDurations>(ee,
				params_.ee_phase_durations_.at(ee),
				params_.ee_in_contact_at_start_.at(ee),
				params_.bound_phase_duration_.first,
				params_.bound_phase_duration_.second);
		vars.push_back(var);
	}

	return vars;
}

NlpFormulation::ContraintPtrVec NlpFormulation::GetConstraints(
		const MySplineHolder& spline_holder) const
{
	ContraintPtrVec constraints;
	for (auto name : params_.constraints_)
		for (auto c : GetConstraint(name, spline_holder))
			constraints.push_back(c);

	return constraints;
}

NlpFormulation::ContraintPtrVec NlpFormulation::GetConstraint(
		Parameters::ConstraintName name, const MySplineHolder& s) const
{
	switch (name)
	{
	case Parameters::TotalTime:
		return MakeTotalTimeConstraint();
	case Parameters::Terrain:
		return MakeTerrainConstraint();
	case Parameters::Force:
		return MakeForceConstraint();
	case Parameters::Swing:
		return MakeSwingConstraint();
	case Parameters::DynamicTorque:
		return MakeDynamicTorqueConstraint(s);
	case Parameters::JointAcc:
		return MakeJointAccConstraint(s);
	default:
		throw std::runtime_error("constraint not defined!");
	}
}

NlpFormulation::ContraintPtrVec NlpFormulation::MakeDynamicTorqueConstraint(
		const MySplineHolder& s) const
{
	auto constraint = std::make_shared<DynamicTorqueConstraint>(wbd_model_,
			params_.GetTotalTime(), params_.dt_constraint_dynamic_, s);

	ContraintPtrVec c = { constraint };

	return c;
}

NlpFormulation::ContraintPtrVec NlpFormulation::MakeTotalTimeConstraint() const
{
	ContraintPtrVec c;
	double T = params_.GetTotalTime();

	for (int ee = 0; ee < params_.GetEECount(); ee++)
	{
		auto duration_constraint = std::make_shared<TotalDurationConstraint>(T,
				ee);
		c.push_back(duration_constraint);
	}

	return c;
}

NlpFormulation::ContraintPtrVec NlpFormulation::MakeTerrainConstraint() const
{
	ContraintPtrVec constraints;

	for (int ee = 0; ee < params_.GetEECount(); ee++)
	{
		auto c = std::make_shared<TerrainConstraint>(terrain_,
				id::EEMotionNodes(ee));
		constraints.push_back(c);
	}

	return constraints;
}

NlpFormulation::ContraintPtrVec NlpFormulation::MakeForceConstraint() const
{
	ContraintPtrVec constraints;

	for (int ee = 0; ee < params_.GetEECount(); ee++)
	{
		auto c = std::make_shared<ForceConstraint>(terrain_,
				params_.force_limit_in_normal_direction_, ee);
		constraints.push_back(c);
	}

	return constraints;
}

NlpFormulation::ContraintPtrVec NlpFormulation::MakeSwingConstraint() const
{
	ContraintPtrVec constraints;

	for (int ee = 0; ee < params_.GetEECount(); ee++)
	{
		auto swing = std::make_shared<SwingConstraint>(id::EEMotionNodes(ee));
		constraints.push_back(swing);
	}

	return constraints;
}

NlpFormulation::ContraintPtrVec NlpFormulation::MakeJointAccConstraint(
		const MySplineHolder& s) const
{
	ContraintPtrVec constraints;

	// Smooth joint motion acceleration
	constraints.push_back(
			std::make_shared<SplineAccConstraint>(s.joint_motion_,
					id::joint_motion_nodes));

	// Smooth joint torque acceleration
	//constraints.push_back(
	//		std::make_shared<SplineAccConstraint>(s.joint_torque_,
	//				id::joint_torque_nodes));

	return constraints;
}

NlpFormulation::ContraintPtrVec NlpFormulation::GetCosts() const
{
	ContraintPtrVec costs;
	for (const auto& pair : params_.costs_)
		for (auto c : GetCost(pair.first, pair.second))
			costs.push_back(c);

	return costs;
}

NlpFormulation::CostPtrVec NlpFormulation::GetCost(
		const Parameters::CostName& name, double weight) const
{
	switch (name)
	{
	case Parameters::ForcesCostID:
		return MakeForcesCost(weight);
	case Parameters::EEMotionCostID:
		return MakeEEMotionCost(weight);
	case Parameters::TorqueCostID:
		return MakeTorqueCost(weight);
	case Parameters::JointVelocityID:
		return MakeJointVelocityCost(weight);
	default:
		throw std::runtime_error("cost not defined!");
	}
}

NlpFormulation::CostPtrVec NlpFormulation::MakeForcesCost(double weight) const
{
	CostPtrVec cost;

	for (int ee = 0; ee < params_.GetEECount(); ee++)
		cost.push_back(
				std::make_shared<NodeCost>(id::EEForceNodes(ee), kPos, Z,
						weight));

	return cost;
}

NlpFormulation::CostPtrVec NlpFormulation::MakeTorqueCost(double weight) const
{
	CostPtrVec cost;

	for (int i = 0; i < params_.num_torques_; i++)
	{
		cost.push_back(
				std::make_shared<NodeCost>(id::joint_torque_nodes, kPos, i,
						weight));
	}

	return cost;
}

NlpFormulation::CostPtrVec NlpFormulation::MakeJointVelocityCost(
		double weight) const
{
	CostPtrVec cost;

	for (int i = 0; i < params_.num_q_; i++)
	{
		cost.push_back(
				std::make_shared<NodeCost>(id::joint_motion_nodes, kVel, i,
						weight));
	}

	return cost;
}

} /* namespace towr */

#endif /* MY_NLP_FORMULATION_H */
