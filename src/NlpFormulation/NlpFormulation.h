#ifndef TOWR_NLP_FACTORY_H_
#define TOWR_NLP_FACTORY_H_

#include <ifopt/variable_set.h>
#include <ifopt/constraint_set.h>
#include <ifopt/cost_term.h>

#include <towr/variables/spline_holder.h>
#include <towr/models/robot_model.h>
#include "../Robots/WBDRobotModel.h"
#include <towr/terrain/height_map.h>

#include "Parameters.h"
#include "../Variables/MySplineHolder.h"

namespace towr
{

/**
 * @defgroup Constraints
 * @brief Constraints of the trajectory optimization problem.
 *
 * These are the constraint sets that characterize legged locomotion.
 *
 * Folder: @ref include/towr/constraints
 */

/**
 * @defgroup Costs
 * @brief Costs of the trajectory optimization problem.
 *
 * These the the cost terms that prioritize certain solutions to the
 * legged locomotion problem.
 *
 * Folder: @ref include/towr/costs
 */

/**
 *
 * @brief A sample combination of variables, cost and constraints.
 *
 * This is _one_ example of how to combine the variables, constraints and costs
 * provided by this library. Additional variables or constraints can be added
 * to the NLP, or existing elements replaced to find a more powerful/general
 * formulation. This formulation was used to generate the motions described
 * in this paper: https://ieeexplore.ieee.org/document/8283570/
 */
class NlpFormulation
{
public:
	using VariablePtrVec = std::vector<ifopt::VariableSet::Ptr>;
	using ContraintPtrVec = std::vector<ifopt::ConstraintSet::Ptr>;
	using CostPtrVec = std::vector<ifopt::CostTerm::Ptr>;
	using EEPos = std::vector<Eigen::Vector3d>;
	using Vector3d = Eigen::Vector3d;
	using VectorXd = Eigen::VectorXd;

	NlpFormulation();
	virtual ~NlpFormulation() = default;

	/**
	 * @brief The ifopt variable sets that will be optimized over.
	 * @param[in/out] builds fully-constructed splines from the variables.
	 */
	VariablePtrVec GetVariableSets(MySplineHolder& spline_holder);

	/**
	 * @brief The ifopt constraints that enforce feasible motions.
	 * @param[in] uses the fully-constructed splines for initialization of constraints.
	 */
	ContraintPtrVec GetConstraints(const MySplineHolder& spline_holder) const;

	/** @brief The ifopt costs to tune the motion. */
	ContraintPtrVec GetCosts() const;

	std::vector<std::pair<double, double>> joint_limits_; 	///< Limits per joint
	std::vector<int> joint_bounds_; ///< Indices of joints which are to be limited

	std::vector<std::pair<double, double>> torque_limits_; 	///< Limits per torque
	std::vector<int> torque_bounds_; ///< Indices of torques which are to be limited

	/// List of points to help start joints guess
	std::vector<std::pair<double, Eigen::VectorXd>> joints_guess_;

	/// List of points to help start end-effector guess
	std::vector<std::pair<double, Eigen::VectorXd>> ee_pos_guess_;

	/// Torque estimation to maintain balance
	Eigen::VectorXd torque_guess_;

	VectorXd initial_joint_pos_;
	VectorXd final_joint_pos_;
	EEPos initial_ee_W_;
	EEPos final_ee_W_;
	WBDRobotModel::Ptr wbd_model_;
	HeightMap::Ptr terrain_;
	Parameters params_;

private:
	// variables
	NodesVariables::Ptr MakeJointVariables() const;
	NodesVariables::Ptr MakeTorqueVariables() const;
	std::vector<NodesVariablesPhaseBased::Ptr> MakeEndeffectorVariables() const;
	std::vector<NodesVariablesPhaseBased::Ptr> MakeForceVariables() const;
	std::vector<PhaseDurations::Ptr> MakeContactScheduleVariables() const;

	// constraints
	ContraintPtrVec GetConstraint(Parameters::ConstraintName name,
			const MySplineHolder& splines) const;
	ContraintPtrVec MakeDynamicTorqueConstraint(const MySplineHolder& s) const;
	ContraintPtrVec MakeTotalTimeConstraint() const;
	ContraintPtrVec MakeTerrainConstraint() const;
	ContraintPtrVec MakeForceConstraint() const;
	ContraintPtrVec MakeSwingConstraint() const;
	ContraintPtrVec MakeJointAccConstraint(const MySplineHolder& s) const;

	// costs
	CostPtrVec GetCost(const Parameters::CostName& id, double weight) const;
	CostPtrVec MakeForcesCost(double weight) const;
	CostPtrVec MakeEEMotionCost(double weight) const;
	CostPtrVec MakeTorqueCost(double weight) const;
	CostPtrVec MakeJointVelocityCost(double weight) const;
};

} /* namespace towr */

#endif /* TOWR_NLP_FACTORY_H_ */
