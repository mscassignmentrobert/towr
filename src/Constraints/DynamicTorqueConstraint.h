#ifndef DYNAMIC_TORQUE_CONSTRAINT_H_
#define DYNAMIC_TORQUE_CONSTRAINT_H_

#include <towr/variables/spline.h>
#include "../Variables/MySplineHolder.h"
#include <towr/variables/euler_converter.h>
#include "../Robots/WBDRobotModel.h"
#include "RobotTimeDiscretizationConstraint.h"

namespace towr
{

/**
 * @brief  Ensure that the optimized motion complies with the system dynamics.
 *
 * TODO: Rework for new joint-motion variable-set
 *
 * At specific time instances along the trajecetory, this class checks the
 * current value of the optimization variables and makes sure that the
 * current robot state (pos,vel) and forces match the current acceleration
 * defined by the system dynamics.
 *
 * The physics-based acceleration is influenced by the robot state as
 *
 *     xdd(t) = f(x(t), xd(t), f(t))
 *
 * The constraint of the optimization variables w is then:
 *
 *     g(t) = acc_spline(t) - xdd(t)
 *          = acc_spline(t) - xdd(x(t), xd(t), f(t))
 *          = acc_spline(w) - xdd(w)
 *
 * All methods are per instance. An instance refers to time (or node) when
 * it is applied.
 *
 * @ingroup Constraints
 */
class DynamicTorqueConstraint: public RobotTimeDiscretizationConstraint
{
public:
	using Vector3d = Eigen::Vector3d;
	using MatrixXd = Eigen::MatrixXd;
	using Vector6d = Eigen::Matrix<double, 6, 1>;

	/**
	 * @brief  Construct a Dynamic constraint
	 * @param model  The system dynamics to enforce (e.g. centroidal, LIP, ...)
	 * @param T   The total duration of the optimization.
	 * @param dt  the discretization interval at which to enforce constraints.
	 * @param spline_holder  A pointer to the current optimization variables.
	 */
	DynamicTorqueConstraint(const WBDRobotModel::Ptr &m, double T, double dt,
			const MySplineHolder &spline_holder);
	virtual ~DynamicTorqueConstraint() = default;

private:

	/**
	 * @brief The row in the overall constraint for this evaluation time.
	 * @param k The index of the constraint evaluation at t=k*dt.
	 * @param i_q Which acceleration dimension this constraint is for.
	 * @return The constraint index in the overall dynamic constraint.
	 */
	int GetRow(int k, int i_q = 0) const;

	void UpdateConstraintAtInstance(double t, int k, VectorXd &g) const
			override;
	void UpdateBoundsAtInstance(double t, int k, VecBound &bounds) const
			override;
	void UpdateJacobianAtInstance(double t, int k, std::string, Jacobian&) const
			override;
};

} /* namespace towr */

#endif /* DYNAMIC_TORQUE_CONSTRAINT_H_ */
