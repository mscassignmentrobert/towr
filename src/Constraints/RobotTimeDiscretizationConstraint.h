#ifndef CONSTRAINTS_ROBOTTIMEDISCRETIZATIONCONSTRAINT_H_
#define CONSTRAINTS_ROBOTTIMEDISCRETIZATIONCONSTRAINT_H_

#include <towr/constraints/time_discretization_constraint.h>
#include "../Variables/MySplineHolder.h"
#include "../Robots/WBDRobotModel.h"

namespace towr
{

class RobotTimeDiscretizationConstraint: public TimeDiscretizationConstraint
{
public:
	/** Constructor */
	RobotTimeDiscretizationConstraint(const WBDRobotModel::Ptr& m, double T,
			double dt, const MySplineHolder& splineholder, const char* name);
	virtual ~RobotTimeDiscretizationConstraint() = default;

protected:

	/** Update model with current time */
	void UpdateModel(double t) const;

	/** Get row of specific constraint */
	virtual int GetRow(int node, int index = 0) const = 0;

	MySplineHolder s_; ///< Simply store the spline holder to have access to all splines

	mutable WBDRobotModel::Ptr model_; ///< Model describing system dynamics
};

} /* namespace towr */

#endif /* CONSTRAINTS_ROBOTTIMEDISCRETIZATIONCONSTRAINT_H_ */
