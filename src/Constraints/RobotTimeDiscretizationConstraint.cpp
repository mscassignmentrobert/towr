#include "RobotTimeDiscretizationConstraint.h"

namespace towr
{

RobotTimeDiscretizationConstraint::RobotTimeDiscretizationConstraint(
		const WBDRobotModel::Ptr& m, double T, double dt,
		const MySplineHolder& splineholder, const char* name) :
		TimeDiscretizationConstraint(T, dt, name)
{
	// Set model
	model_ = m;

	// link with up-to-date spline variables
	s_ = splineholder;
}

/**
 * Update model with current instance
 */
void RobotTimeDiscretizationConstraint::UpdateModel(double t) const
{
	model_->SetCurrent(s_, t);
}

}
/* namespace towr */
