#include "DynamicTorqueConstraint.h"

#include <towr/variables/variable_names.h>
#include <towr/variables/cartesian_dimensions.h>
#include "../Variables/MyVariableNames.h"
#include <iostream>

namespace towr
{

DynamicTorqueConstraint::DynamicTorqueConstraint(const WBDRobotModel::Ptr &m,
		double T, double dt, const MySplineHolder &splineholder) :
		RobotTimeDiscretizationConstraint(m, T, dt, splineholder, "dynamic-torque")
{
	// Constraint rows are first grouped by ddq, then by node
	SetRows(GetNumberOfNodes() * model_->size_q_);
}

/**
 * Get row of specific constraint
 */
int DynamicTorqueConstraint::GetRow(int k, int i_q) const
{
	return model_->size_q_ * k + i_q;
}

/**
 * Get constraint value for one specific node and dimension
 *
 * This method is called by `GetValues()` and provides the actual values.
 */
void DynamicTorqueConstraint::UpdateConstraintAtInstance(double t, int k,
		VectorXd &g) const
{
	UpdateModel(t);
	g.segment(GetRow(k), model_->size_q_) = model_->GetDynamicViolation();
}

/**
 * Set bound at time instance
 *
 * Bounds are simply always zero.
 */
void DynamicTorqueConstraint::UpdateBoundsAtInstance(double t, int k,
		VecBound &bounds) const
{
	for (int i_q = 0; i_q < model_->size_q_; i_q++)
	{
		bounds.at(GetRow(k, i_q)) = ifopt::BoundZero;
	}
}

/**
 * Set Jacobian at time instance
 *
 * A section of the complete Jacobian is now specified.
 * This function is called multiple times, each with different variable
 * set names.
 */
void DynamicTorqueConstraint::UpdateJacobianAtInstance(double t, int k,
		std::string var_set, Jacobian &jac) const
{
	UpdateModel(t);

	// Fill in a specific set of rows belonging to this instance
	int n = jac.cols(); // For all columns, i.e. w.r.t. to all variables
	Jacobian jac_dynamics(model_->size_q_, n); // Section which is being written now

	// sensitivity of dynamic constraint w.r.t base variables.
	if (var_set == id::base_lin_nodes)
	{
		jac_dynamics = model_->GetJacDynamicsWrtBaseLin(
				s_.base_linear_->GetJacobianWrtNodes(t, kPos),
				s_.base_linear_->GetJacobianWrtNodes(t, kVel),
				s_.base_linear_->GetJacobianWrtNodes(t, kAcc));
	}

	// sensitivity of dynamic constraint w.r.t ee pos.
	if (var_set == id::EEMotionNodes(0))
	{
		jac_dynamics = model_->GetJacDynamicsWrtEEPos(
				s_.ee_motion_[0]->GetJacobianWrtNodes(t, kPos),
				s_.ee_motion_[0]->GetJacobianWrtNodes(t, kVel),
				s_.ee_motion_[0]->GetJacobianWrtNodes(t, kAcc));
	}

	// sensitivity of dynamic constraint w.r.t joint motion.
	if (var_set == id::joint_motion_nodes)
	{
		jac_dynamics = model_->GetJacDynamicsWrtJointMotion(
				s_.joint_motion_->GetJacobianWrtNodes(t, kPos),
				s_.joint_motion_->GetJacobianWrtNodes(t, kVel),
				s_.joint_motion_->GetJacobianWrtNodes(t, kAcc));
	}

	// sensitivity of dynamic constraint w.r.t. joint torques
	if (var_set == id::joint_torque_nodes)
	{
		jac_dynamics = model_->GetJacDynamicsWrtJointTorque(
				s_.joint_torque_->GetJacobianWrtNodes(t, kPos));
	}

	// sensitivity of dynamic constraint w.r.t end-effector force.
	if (var_set == id::EEForceNodes(0))
	{
		jac_dynamics = model_->GetJacDynamicsWrtForce(
				s_.ee_force_[0]->GetJacobianWrtNodes(t, kPos));
	}

	jac.middleRows(GetRow(k), model_->size_q_) = jac_dynamics; // Insert section in big Jacobian
	// ^ IDE shows error but it compiles just fine
}

} /* namespace towr */
