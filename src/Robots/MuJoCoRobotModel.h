#ifndef ROBOTS_MUJOCOROBOTMODEL_H_
#define ROBOTS_MUJOCOROBOTMODEL_H_

#include "WBDRobotModel.h"
#include "../MuJoCoTools/MuJoCoModel.hpp"

namespace towr
{

/**
 * Robot model defined through MuJoCo
 */
class MuJoCoRobotModel: public WBDRobotModel
{
public:
	using MatrixXd = Eigen::MatrixXd;

	/** Construct model from file */
	MuJoCoRobotModel(const char *file, int size_q_, int size_u_, int size_p_);
	virtual ~MuJoCoRobotModel() = default;

	/** Update internal calculations, to be executed after SetCurrent() */
	void Update();

	/** Get error in dynamics */
	virtual VectorXd GetDynamicViolation() const override;

	/** Return reference to MuJoCo model */
	MuJoCoModel& GetMuJoCoModel();

protected:

	/**
	 * MuJoCo model object
	 *
	 * `mutable` since it's internal states are to be updated
	 */
	mutable MuJoCoModel model_;

	VectorXd ddq_calculated_; ///< Calculated acceleration

	/** Acceleration jacobians */
	Jac ddq_diff_q_, ddq_diff_dq_, ddq_diff_u_;
};

} /* namespace towr */

#endif /* ROBOTS_MUJOCOROBOTMODEL_H_ */
