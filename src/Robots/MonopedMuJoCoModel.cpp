#include "MonopedMuJoCoModel.h"
#include <iostream>

namespace towr
{

/**
 * Constructor
 */
MonopedMuJoCoModel::MonopedMuJoCoModel() :
		MuJoCoRobotModel("Debug/monoped_foot.xml", /*size_q*/5, /*size_u*/4, /*size_p*/
				3)
{
	// Initializer lists does most of it

	mj_id_base_ = mj_name2id(model_.getModel(), mjOBJ_BODY, "base");

	assert(mj_id_base_ > -1);

	// Intialize matrix
	p_calcualted_ = VectorXd(size_p_);
	p_diff_q_ = Jac(size_p_, size_q_);

	p_calcualted_.setZero();
	p_diff_q_.setZero();
}

/**
 * Update current instance through splines
 *
 * By placing this code inside the robot class, each class
 * can have their own mapping to generalized coordinates.
 */
void MonopedMuJoCoModel::SetCurrent(const MySplineHolder& s, double t)
{
	// Original data
	Vector3d ee_W = s.ee_motion_[0]->GetPoint(t).p();
	Vector3d d_ee_W = s.ee_motion_[0]->GetPoint(t).v();
	Vector3d dd_ee_W = s.ee_motion_[0]->GetPoint(t).a();
	VectorXd joints = s.joint_motion_->GetPoint(t).p();
	VectorXd d_joints = s.joint_motion_->GetPoint(t).v();
	VectorXd dd_joints = s.joint_motion_->GetPoint(t).a();
	VectorXd torque = s.joint_torque_->GetPoint(t).p();
	Vector3d force = s.ee_force_[0]->GetPoint(t).p();

	// Build official states
	q_ << ee_W[0], ee_W[2], joints[0], joints[1], joints[2];
	dq_ << d_ee_W[0], d_ee_W[2], d_joints[0], d_joints[1], d_joints[2];
	ddq_ << dd_ee_W[0], dd_ee_W[2], dd_joints[0], dd_joints[1], dd_joints[2];
	u_ << torque[0], torque[1], force[0], force[2];

	// Propagate states
	MuJoCoRobotModel::Update();
}

// --------------------------

/**
 * Get jacobian w.r.t. ee position
 */
MonopedMuJoCoModel::Jac MonopedMuJoCoModel::GetJacDynamicsWrtEEPos(
		const Jac& jac_ee_pos, const Jac& jac_ee_vel,
		const Jac& jac_ee_acc) const
{
	// Jacobian from joint coordinates to base position
	Jac jac_q_base(size_q_, 3);

	jac_q_base.insert(0, 0) = 1.0;
	jac_q_base.insert(1, 2) = 1.0;

	Jac jac1 = ddq_diff_q_ * jac_q_base * jac_ee_pos;
	Jac jac2 = ddq_diff_dq_ * jac_q_base * jac_ee_vel;
	Jac jac3 = jac_q_base * jac_ee_acc;
	return -jac1 - jac2 + jac3;
}

/**
 * Get jacobian w.r.t. joint motion
 */
MonopedMuJoCoModel::Jac MonopedMuJoCoModel::GetJacDynamicsWrtJointMotion(
		const Jac& jac_joint_pos, const Jac& jac_joint_vel,
		const Jac& jac_joint_acc) const
{
	// Jacobian from q to actual joint coordinates
	Jac jac_q_angles(size_q_, 3);

	jac_q_angles.insert(2, 0) = 1.0;
	jac_q_angles.insert(3, 1) = 1.0;
	jac_q_angles.insert(4, 2) = 1.0;

	Jac jac1 = ddq_diff_q_ * jac_q_angles * jac_joint_pos;
	Jac jac2 = ddq_diff_dq_ * jac_q_angles * jac_joint_vel;
	Jac jac3 = jac_q_angles * jac_joint_acc;
	return -jac1 - jac2 + jac3;
}

/**
 * Get jacobian w.r.t. joint torques
 */
MonopedMuJoCoModel::Jac MonopedMuJoCoModel::GetJacDynamicsWrtJointTorque(
		const Jac& jac_joint_torque) const
{
	// Jacobian from u to joint torques
	Jac jac_u_tau(size_u_, 2);

	jac_u_tau.insert(0, 0) = 1.0;
	jac_u_tau.insert(1, 1) = 1.0;

	return -ddq_diff_u_ * jac_u_tau * jac_joint_torque;
}

/**
 * Get jacobian w.r.t. end-effector force
 */
MonopedMuJoCoModel::Jac MonopedMuJoCoModel::GetJacDynamicsWrtForce(
		const Jac& jac_force) const
{
	// Jacobian from u to foot force
	Jac jac_u_f(size_u_, 3);

	jac_u_f.insert(2, 0) = 1.0;
	jac_u_f.insert(3, 2) = 1.0;

	return -ddq_diff_u_ * jac_u_f * jac_force;
}

// --------------------------

/**
 * Error in kinematics
 *
 * Error = actual base position - calculated base position
 */
MonopedMuJoCoModel::VectorXd MonopedMuJoCoModel::GetKinematicViolation() const
{
	/*
	 Vector3d rot_vec_ini = {0.0, 0.0, 1.0};
	 Vector3d rot_vec;
	 mju_rotVecQuat(rot_vec.data(), rot_vec_ini.data(), sensor_base_quat_);
	 // Calculate y-euler angle (x and y are flipped on purpose to match spatial)
	 double angle_y = std::atan2(rot_vec[0], rot_vec[2]);
	 */

	return p_ - p_calcualted_;
}

// 204 errors

/**
 * Jac of kinematics violation w.r.t. base position
 */
MonopedMuJoCoModel::Jac MonopedMuJoCoModel::GetJacKinematicsWrtBaseLin(
		const Jac& jac_base_lin_pos) const
{
	// Jacobian from joint coordinates to base position
	Jac jac_p_base(size_p_, 3);

	jac_p_base.insert(1, 0) = 1.0;
	jac_p_base.insert(2, 2) = 1.0;

	return jac_p_base * jac_base_lin_pos;
}

/**
 * Jac of kinematics violation w.r.t. ee position
 */
MonopedMuJoCoModel::Jac MonopedMuJoCoModel::GetJacKinematicsWrtEEPos(
		const Jac& jac_ee_pos) const
{
	// Jacobian from joint coordinates to base position
	Jac jac_q_ee(size_q_, 3);

	jac_q_ee.insert(0, 0) = 1.0;
	jac_q_ee.insert(1, 2) = 1.0;

	return -p_diff_q_ * jac_q_ee * jac_ee_pos;
}

/**
 * Jac of kinematics violation w.r.t. joint motion
 */
MonopedMuJoCoModel::Jac MonopedMuJoCoModel::GetJacKinematicsWrtJointMotion(
		const Jac& jac_joint_pos) const
{
	// Jacobian from joint coordinates to base position
	Jac jac_q_joint(size_q_, 3);

	jac_q_joint.insert(2, 0) = 1.0;
	jac_q_joint.insert(3, 1) = 1.0;
	jac_q_joint.insert(4, 2) = 1.0;

	return -p_diff_q_ * jac_q_joint * jac_joint_pos;
}

/**
 * Jac of kinematics violation w.r.t. base angle
 */
MonopedMuJoCoModel::Jac MonopedMuJoCoModel::GetJacKinematicsWrtBaseAng(
		const Jac& jac_base_ang_pos) const
{
	Jac jac_p_base_ang(size_p_, 3);

	jac_p_base_ang.insert(0, 1) = 1.0;

	return jac_p_base_ang * jac_base_ang_pos;
}

// --------------------------

} /* namespace towr */

