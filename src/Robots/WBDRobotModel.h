#ifndef ROBOTS_WBDROBOTMODEL_H_
#define ROBOTS_WBDROBOTMODEL_H_

#include <memory>
#include <Eigen/Dense>
#include <Eigen/Sparse>

#include "../Variables/MySplineHolder.h"

namespace towr
{

/**
 * Class for robot model with whole-body dynamics
 *
 * Class combines kinematics and dynamics.
 */
class WBDRobotModel
{
public:
	using Ptr = std::shared_ptr<WBDRobotModel>;
	using VectorXd = Eigen::VectorXd;
	using Vector3d = Eigen::Vector3d;
	using Jac = Eigen::SparseMatrix<double, Eigen::RowMajor>;

	WBDRobotModel(int size_q, int size_u, int size_p_);
	virtual ~WBDRobotModel() = default;

	/** Update internal states for current instance, though spline holder */
	virtual void SetCurrent(const MySplineHolder& s, double t) = 0;

	// --------------------------

	/** Get error in dynamics */
	virtual VectorXd GetDynamicViolation() const = 0;

	/**
	 * @brief How the base position affects the dynamic violation.
	 * @param jac_base_lin_pos  The 3xn Jacobian of the base linear position.
	 * @param jac_base_lin_acc  The 3xn Jacobian of the base linear acceleration.
	 *
	 * @return The 6xn Jacobian of dynamic violations with respect to
	 *         variables defining the base linear spline (e.g. node values).
	 */
	virtual Jac GetJacDynamicsWrtBaseLin(const Jac& jac_base_lin_pos,
			const Jac& jac_base_lin_vel, const Jac& jac_base_lin_acc) const;

	/**
	 * @brief How end-effector position affect dynamics.
	 */
	virtual Jac GetJacDynamicsWrtEEPos(const Jac& jac_ee_pos,
			const Jac& jac_ee_vel, const Jac& jac_ee_acc) const;

	/**
	 * @brief How the joint torques affect the dynamic violation.
	 */
	virtual Jac GetJacDynamicsWrtJointTorque(const Jac& jac_joint_torque) const;

	/**
	 * @brief How the endeffector forces affect the dynamic violation.
	 */
	virtual Jac GetJacDynamicsWrtForce(const Jac& jac_force) const;

	/**
	 * @brief How the joint motions affect the dynamic violation.
	 */
	virtual Jac GetJacDynamicsWrtJointMotion(const Jac& jac_joint_pos,
			const Jac& jac_joint_vel, const Jac& jac_joint_acc) const;

	// --------------------------

	/** Get kinematics violation */
	virtual VectorXd GetKinematicViolation() const = 0;

	/**
	 * @brief How base position affect kinematics
	 */
	virtual Jac GetJacKinematicsWrtBaseLin(const Jac& jac_base_lin_pos) const;

	/**
	 * @brief How base rotation affect kinematics
	 */
	virtual Jac GetJacKinematicsWrtBaseAng(const Jac& jac_base_ang_pos) const;

	/**
	 * @brief How end-effector position affect kinematics.
	 */
	virtual Jac GetJacKinematicsWrtEEPos(const Jac& jac_ee_pos) const;

	/**
	 * @brief How the joint motions affect the kinematics.
	 */
	virtual Jac GetJacKinematicsWrtJointMotion(const Jac& jac_joint_pos) const;

	// --------------------------

	const VectorXd& GetCurrentDynamics() const;

	double GetTotalMass() const;
	double GetG() const;

	int size_q_; ///< Number of independent coordinates
	int size_u_; ///< Number of torque inputs
	int size_p_; ///< Number of kinematic end-effector coordinates

protected:

	VectorXd q_, dq_, u_; ///< States and inputs of the system
	VectorXd ddq_; ///< Current acceleration (*not* the computed one)
	VectorXd p_; ///< Current kinematic end-effector

	double g_, m_total_; ///< g and total mass to help initial guess of reaction force
};

} /* namespace towr */

#endif /* ROBOTS_WBDROBOTMODEL_H_ */
