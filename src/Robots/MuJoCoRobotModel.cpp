#include <string>
#include <cassert>
#include <mujoco/mujoco.h>
#include "MuJoCoRobotModel.h"

namespace towr
{

/**
 * Constructor
 *
 * size_q and size_u can also be extracted from the model, however,
 * the parent constructor already requires them.
 */
MuJoCoRobotModel::MuJoCoRobotModel(const char *file, int size_q, int size_u, int size_p) :
		WBDRobotModel(size_q, size_u, size_p), model_(file)
{
	mjModel *m = model_.getModel();

	assert(m->nv == size_q_); // Make sure nDOF is correct
	assert(m->nu == size_u_); // Make sure nDOF is correct

	ddq_calculated_ = VectorXd(size_q_);

	ddq_diff_q_ = Jac(size_q_, size_q_);
	ddq_diff_dq_ = Jac(size_q_, size_q_);
	ddq_diff_u_ = Jac(size_q_, size_u_);

	// Get total mass and gravity from model
	g_ = mju_norm3(m->opt.gravity);
	m_total_ = mj_getTotalmass(m);
}

/**
 * Get reference to MuJoCo model
 */
MuJoCoModel& MuJoCoRobotModel::GetMuJoCoModel()
{
	return model_;
}

/**
 * Update internal calculations
 */
void MuJoCoRobotModel::Update()
{
	// Bring model to current instance
	model_.reset_data(q_.data(), dq_.data(), u_.data());

	model_.get_dynamics(ddq_calculated_.data());

	MatrixXd ddq_diff_q(size_q_, size_q_), ddq_diff_dq(size_q_, size_q_), ddq_diff_u(size_q_, size_u_);
	model_.get_dynamics_diff_qpos(ddq_diff_q.data());
	model_.get_dynamics_diff_qvel(ddq_diff_dq.data());
	model_.get_dynamics_diff_u(ddq_diff_u.data());

	ddq_diff_q_ = ddq_diff_q.sparseView();
	ddq_diff_dq_ = ddq_diff_dq.sparseView();
	ddq_diff_u_ = ddq_diff_u.sparseView();
}

/**
 * Get error in dynamics
 *
 * Error is defined as:
 * 		ddq_current - ddq_calculated
 */
MuJoCoRobotModel::VectorXd MuJoCoRobotModel::GetDynamicViolation() const
{
	// Acceleration error
	return ddq_ - ddq_calculated_;
}

} /* namespace towr */
