#include <cmath>
#include "MonopedModel.h"

#include "autogen/dynamics_F.h"

namespace towr
{

/**
 * Constructor
 */
MonopedModel::MonopedModel() :
		WBDRobotModel(/* size_q */5, /* size_u */4, /* size_p */3)
{
	// Allocate vectors and matrices
	M_ = MatrixXd(size_q_, size_q_);
	dynamics_ = VectorXd(size_q_);

	base_angle_ = 0.0;

	jac_q_ = Jac(size_q_, size_q_);
	jac_dq_ = Jac(size_q_, size_q_);
	jac_ddq_ = Jac(size_q_, size_q_);

	L2_ = 0.5;
	L3_ = 0.5;
}

/**
 * Extend SetCurrent
 */
void MonopedModel::SetCurrent(const MySplineHolder& s, double t)
{
	// Original data
	Vector3d base_W = s.base_linear_->GetPoint(t).p();
	Vector3d d_base_W = s.base_linear_->GetPoint(t).v();
	Vector3d dd_base_W = s.base_linear_->GetPoint(t).a();
	VectorXd joints = s.joint_motion_->GetPoint(t).p();
	VectorXd d_joints = s.joint_motion_->GetPoint(t).v();
	VectorXd dd_joints = s.joint_motion_->GetPoint(t).a();
	VectorXd torque = s.joint_torque_->GetPoint(t).p();
	Vector3d force = s.ee_force_[0]->GetPoint(t).p();
	Vector3d ee_W = s.ee_motion_[0]->GetPoint(t).p();
	Vector3d base_angles = s.base_angular_->GetPoint(t).p(); // X, Y, Z Euler angles

	VectorXd q(size_q_), dq(size_q_), ddq(size_q_), u(size_u_);

	// Build official states
	q_ << base_W[0], base_W[2], joints[0], joints[1], joints[2];
	dq_ << d_base_W[0], d_base_W[2], d_joints[0], d_joints[1], d_joints[2];
	ddq_ << dd_base_W[0], dd_base_W[2], dd_joints[0], dd_joints[1], dd_joints[2];
	u_ << torque[0], torque[1], force[0], force[2];
	p_ << base_angles[1], ee_W[0], ee_W[2];

	// Calculate matrices
	Update();
}

/**
 * Update state propagation
 */
void MonopedModel::Update()
{
	// Fill matrices and vectors
	// TODO: Add check to see if these need to be recalculated at all
	monoped::dynamics_F(q_.data(), dq_.data(), u_.data(), dynamics_.data());
	monoped::dynamics_M(q_.data(), M_.data());

	// d (M*ddq) / d q
	MatrixXd Mq_diff_q(size_q_, size_q_);
	monoped::dynamics_Mq_diff_q(q_.data(), dq_.data(), ddq_.data(), u_.data(),
			Mq_diff_q.data());

	// d F / d q
	MatrixXd dynamics_diff_q(size_q_, size_q_);
	monoped::dynamics_F_diff_q(q_.data(), dq_.data(), u_.data(),
			dynamics_diff_q.data());

	// d F / d dq
	MatrixXd dynamics_diff_dq(size_q_, size_q_);
	monoped::dynamics_F_diff_dq(q_.data(), dq_.data(), u_.data(),
			dynamics_diff_dq.data());

	jac_q_ = Mq_diff_q.sparseView() - dynamics_diff_q.sparseView();
	jac_dq_ = -dynamics_diff_dq.sparseView();
	jac_ddq_ = M_.sparseView();

	// d F / d f
	MatrixXd dynamics_diff_u(size_q_, size_u_);
	monoped::dynamics_F_diff_u(q_.data(), dq_.data(), u_.data(),
			dynamics_diff_u.data());
	dynamics_diff_u_ = dynamics_diff_u.sparseView();
}

/**
 * Get error in dynamics
 */
MonopedModel::VectorXd MonopedModel::GetDynamicViolation() const
{
	// Get torque error
	return M_ * ddq_ - dynamics_;
}

/**
 * Get jacobian w.r.t. base linear nodes
 */
MonopedModel::Jac MonopedModel::GetJacDynamicsWrtBaseLin(
		const Jac& jac_base_lin_pos, const Jac& jac_base_lin_vel,
		const Jac& jac_base_lin_acc) const
{
	/*
	 * First convert dense joint jacobians to sparse jacobians for base only
	 */
	Jac jac_q_base(size_q_, 3), jac_dq_base(size_q_, 3), jac_ddq_base(size_q_,
			3);

	// Copy first and third columns
	for (int i = 0; i < size_q_; i++)
	{
		jac_q_base.coeffRef(i, 0) = jac_q_.coeff(i, 0);
		jac_q_base.coeffRef(i, 2) = jac_q_.coeff(i, 1);

		jac_dq_base.coeffRef(i, 0) = jac_dq_.coeff(i, 0);
		jac_dq_base.coeffRef(i, 2) = jac_dq_.coeff(i, 1);

		jac_ddq_base.coeffRef(i, 0) = jac_ddq_.coeff(i, 0);
		jac_ddq_base.coeffRef(i, 2) = jac_ddq_.coeff(i, 1);
	}

	return jac_q_base * jac_base_lin_pos + jac_dq_base * jac_base_lin_vel
			+ jac_ddq_base * jac_base_lin_acc;
}

/**
 * Get jacobian w.r.t. joint torques
 */
MonopedModel::Jac MonopedModel::GetJacDynamicsWrtJointTorque(
		const Jac& jac_joint_torque) const
{
	/*
	 * First convert dense joint jacobians to sparse jacobians for joint torques only
	 */
	Jac jac_tau(size_q_, 2);

	// Copy first and second columns
	for (int i = 0; i < size_q_; i++)
	{
		jac_tau.coeffRef(i, 0) = dynamics_diff_u_.coeff(i, 0);
		jac_tau.coeffRef(i, 1) = dynamics_diff_u_.coeff(i, 1);
	}

	return -jac_tau * jac_joint_torque;
}

/**
 * Get jacobian w.r.t. end-effector force
 */
MonopedModel::Jac MonopedModel::GetJacDynamicsWrtForce(
		const Jac& jac_force) const
{
	Jac jac_f(size_q_, 3);

	// Copy first and second columns
	for (int i = 0; i < size_q_; i++)
	{
		jac_f.coeffRef(i, 0) = dynamics_diff_u_.coeff(i, 2);
		jac_f.coeffRef(i, 2) = dynamics_diff_u_.coeff(i, 3);
	}

	return -jac_f * jac_force;
}

/**
 * Get jacobian w.r.t. joint motion
 */
MonopedModel::Jac MonopedModel::GetJacDynamicsWrtJointMotion(
		const Jac& jac_joint_pos, const Jac& jac_joint_vel,
		const Jac& jac_joint_acc) const
{
	Jac jac_q_joints(size_q_, 3), jac_dq_joints(size_q_, 3), jac_ddq_joints(
			size_q_, 3);

	// Copy last three columns
	for (int i = 0; i < size_q_; i++)
	{
		jac_q_joints.coeffRef(i, 0) = jac_q_.coeff(i, 2);
		jac_q_joints.coeffRef(i, 1) = jac_q_.coeff(i, 3);
		jac_q_joints.coeffRef(i, 2) = jac_q_.coeff(i, 4);

		jac_dq_joints.coeffRef(i, 0) = jac_dq_.coeff(i, 2);
		jac_dq_joints.coeffRef(i, 1) = jac_dq_.coeff(i, 3);
		jac_dq_joints.coeffRef(i, 2) = jac_dq_.coeff(i, 4);

		jac_ddq_joints.coeffRef(i, 0) = jac_ddq_.coeff(i, 2);
		jac_ddq_joints.coeffRef(i, 1) = jac_ddq_.coeff(i, 3);
		jac_ddq_joints.coeffRef(i, 2) = jac_ddq_.coeff(i, 4);
	}

	return jac_q_joints * jac_joint_pos + jac_dq_joints * jac_joint_vel
			+ jac_ddq_joints * jac_joint_acc;
}

/**
 * Get error in kinematics
 *
 * The error for this model is in the end-effector position:
 *
 * 		p_current - p_forward(q)
 *
 * 	Where p is the position of the foot
 */
MonopedModel::VectorXd MonopedModel::GetKinematicViolation() const
{
	VectorXd p_calculated(size_p_);
	p_calculated << q_[2],									// theta1
	q_[0] - L2_ * std::sin(q_[3]) - L3_ * std::sin(q_[4]),	// x_e
	q_[1] - L2_ * std::cos(q_[3]) - L3_ * std::cos(q_[4]);	// z_e

	return p_ - p_calculated;
}

// 501 errors

/**
 * Get jacobian w.r.t. base linear nodes
 */
MonopedModel::Jac MonopedModel::GetJacKinematicsWrtBaseLin(
		const Jac& jac_base_lin_pos) const
{
	Jac jac_p_base(size_p_, 3);

	jac_p_base.insert(1, 0) = 1.0;
	jac_p_base.insert(2, 2) = 1.0;

	return -jac_p_base * jac_base_lin_pos;
}

/**
 * Get jacobian w.r.t. base angular nodes
 */
MonopedModel::Jac MonopedModel::GetJacKinematicsWrtBaseAng(
		const Jac& jac_base_ang_pos) const
{
	Jac jac_p_ang(size_p_, 3);

	jac_p_ang.insert(0, 1) = 1.0; // Only y

	return jac_p_ang * jac_base_ang_pos;
}

/**
 * Get jacobian w.r.t. ee position
 */
MonopedModel::Jac MonopedModel::GetJacKinematicsWrtEEPos(
		const Jac& jac_ee_pos) const
{
	Jac jac_p_ee(size_p_, 3);

	jac_p_ee.insert(1, 0) = 1.0;
	jac_p_ee.insert(2, 2) = 1.0;

	return jac_p_ee * jac_ee_pos;
}

/**
 * Get jacobian w.r.t. joints
 */
MonopedModel::Jac MonopedModel::GetJacKinematicsWrtJointMotion(
		const Jac& jac_joint_pos) const
{
	Jac jac_p_joints(size_p_, 3);

	jac_p_joints.insert(0, 0) = 1; // d q1 / d q1
	jac_p_joints.insert(1, 1) = -L2_ * std::cos(q_[3]); // d x_e / d q2
	jac_p_joints.insert(1, 2) = -L3_ * std::cos(q_[4]); // d x_e / d q3
	jac_p_joints.insert(2, 1) = L2_ * std::sin(q_[3]); // d z_e / d q2
	jac_p_joints.insert(2, 2) = L3_ * std::sin(q_[4]); // d z_e / d q3

	return -jac_p_joints * jac_joint_pos;
}

} /* namespace towr */
