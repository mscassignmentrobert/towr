#ifndef ROBOTS_MONOPEDMODEL_H_
#define ROBOTS_MONOPEDMODEL_H_

#include "WBDRobotModel.h"

#include <memory>
#include <vector>

#include <Eigen/Dense>
#include <Eigen/Sparse>

namespace towr
{

/**
 * Monoped model based on code generated from MATLAB
 */
class MonopedModel: public WBDRobotModel
{
public:
	using VectorXd = Eigen::VectorXd;
	using MatrixXd = Eigen::MatrixXd;

	MonopedModel();
	virtual ~MonopedModel() = default;

	/** Get error in dynamics */
	VectorXd GetDynamicViolation() const override;

	/** Update internal states for current instance, through spline holder */
	void SetCurrent(const MySplineHolder& s, double t) override;

	/** Update internal calculations, to be executed after SetCurrent() */
	void Update();

	// --------------------------

	/** Jacobian w.r.t. base linear */
	Jac GetJacDynamicsWrtBaseLin(const Jac& jac_base_lin_pos,
			const Jac& jac_base_lin_vel, const Jac& jac_base_lin_acc) const
					override;

	/** How the joint torques affect the dynamic violation */
	Jac GetJacDynamicsWrtJointTorque(const Jac& jac_joint_torque) const
			override;

	/**
	 * @brief How the endeffector forces affect the dynamic violation.
	 */
	Jac GetJacDynamicsWrtForce(const Jac& jac_force) const override;

	/**
	 * @brief How the joint motions affect the dynamic violation.
	 */
	Jac GetJacDynamicsWrtJointMotion(const Jac& jac_joint_pos,
			const Jac& jac_joint_vel, const Jac& jac_joint_acc) const override;

	// --------------------------

	/** Get kinematics violation */
	VectorXd GetKinematicViolation() const override;

	/**
	 * @brief How base position affect kinematics
	 */
	Jac GetJacKinematicsWrtBaseLin(const Jac& jac_base_lin_pos) const override;

	/**
	 * @brief How base rotation affect kinematics
	 */
	Jac GetJacKinematicsWrtBaseAng(const Jac& jac_base_ang_pos) const override;

	/**
	 * @brief How end-effector position affect kinematics.
	 */
	Jac GetJacKinematicsWrtEEPos(const Jac& jac_ee_pos) const override;

	/**
	 * @brief How the joint motions affect the kinematics.
	 */
	Jac GetJacKinematicsWrtJointMotion(const Jac& jac_joint_pos) const override;

	// --------------------------

private:

	MatrixXd M_; 			///< Computed mass matrix
	VectorXd dynamics_; 	///< Computed dynamics

	double base_angle_; 	///< Current angle of the base

	Jac jac_q_, jac_dq_, jac_ddq_; ///< Jacobians of ddq
	Jac dynamics_diff_u_;

	double L2_, L3_;
};

} /* namespace towr */

#endif /* ROBOTS_MONOPEDMODEL_H_ */
