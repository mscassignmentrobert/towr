#include "WBDRobotModel.h"

namespace towr
{

/**
 * Constructor
 */
WBDRobotModel::WBDRobotModel(int size_q, int size_u, int size_p)
{
	size_q_ = size_q;
	size_u_ = size_u;
	size_p_ = size_p;

	// Allocate vectors and matrices
	q_ = VectorXd(size_q_);
	dq_ = VectorXd(size_q_);
	ddq_ = VectorXd(size_q_);
	u_ = VectorXd(size_u_);
	p_ = VectorXd(size_p_);

	g_ = 9.81;
	m_total_ = 0.0;
}

// --------------------------

WBDRobotModel::Jac WBDRobotModel::GetJacDynamicsWrtBaseLin(
		const Jac& jac_base_lin_pos, const Jac& jac_base_lin_vel,
		const Jac& jac_base_lin_acc) const
{
	Jac jac(size_q_, jac_base_lin_pos.cols());
	return jac; // Empty
}

WBDRobotModel::Jac WBDRobotModel::GetJacDynamicsWrtEEPos(const Jac& jac_ee_pos,
		const Jac& jac_ee_vel, const Jac& jac_ee_acc) const
{
	Jac jac(size_q_, jac_ee_pos.cols());
	return jac; // Empty
}

WBDRobotModel::Jac WBDRobotModel::GetJacDynamicsWrtJointTorque(
		const Jac& jac_joint_torque) const
{
	Jac jac(size_q_, jac_joint_torque.cols());
	return jac; // Empty
}

WBDRobotModel::Jac WBDRobotModel::GetJacDynamicsWrtForce(
		const Jac& jac_force) const
{
	Jac jac(size_q_, jac_force.cols());
	return jac; // Empty
}

WBDRobotModel::Jac WBDRobotModel::GetJacDynamicsWrtJointMotion(const Jac& jac_joint_pos,
		const Jac& jac_joint_vel, const Jac& jac_joint_acc) const
{
	Jac jac(size_q_, jac_joint_pos.cols());
	return jac; // Empty
}

// --------------------------

WBDRobotModel::Jac WBDRobotModel::GetJacKinematicsWrtBaseLin(const Jac& jac_base_lin_pos) const
{
	Jac jac(size_q_, jac_base_lin_pos.cols());
	return jac; // Empty
}

WBDRobotModel::Jac WBDRobotModel::GetJacKinematicsWrtBaseAng(const Jac& jac_base_ang_pos) const
{
	Jac jac(size_q_, jac_base_ang_pos.cols());
	return jac; // Empty
}

WBDRobotModel::Jac WBDRobotModel::GetJacKinematicsWrtEEPos(const Jac& jac_ee_pos) const
{
	Jac jac(size_q_, jac_ee_pos.cols());
	return jac; // Empty
}

WBDRobotModel::Jac WBDRobotModel::GetJacKinematicsWrtJointMotion(const Jac& jac_joint_pos) const
{
	Jac jac(size_q_, jac_joint_pos.cols());
	return jac; // Empty
}

// --------------------------

const WBDRobotModel::VectorXd& WBDRobotModel::GetCurrentDynamics() const
{
	return ddq_;
}


double WBDRobotModel::GetTotalMass() const
{
	return m_total_;
}


double WBDRobotModel::GetG() const
{
	return g_;
}

} /* namespace towr */
