#ifndef ROBOTS_MONOPEDMUJOCOMODEL_H_
#define ROBOTS_MONOPEDMUJOCOMODEL_H_

#include "MuJoCoRobotModel.h"

namespace towr
{

/**
 * Class for Monoped, based on MuJoCo model
 *
 * Coordinates:
 *
 * 		q = [
 *			x_e 	// x of foot
 *			z_e		// z of foot
 *			q1		// Angle of lower leg to ground
 *			q2		// Angle of upper leg to lower leg
 *			q3		// Angle of body to upper leg
 *		]
 *
 *		u = [
 *			tau1 	// torque from lower to upper leg (positive for upper leg)
 *			tau2 	// torque from upper leg to body (positive for body)
 *			fx		// force on foot +x
 *			fz		// force on foot +z
 *		]
 *
 *		p = [
 *			base_angle	// y-euler angle of the base
 *			x_b		// x of base
 *			z_b		// z of base
 *		]
 */
class MonopedMuJoCoModel: public MuJoCoRobotModel
{
public:
	MonopedMuJoCoModel();
	virtual ~MonopedMuJoCoModel() = default;

	/** Update internal states for current instance, though spline holder */
	void SetCurrent(const MySplineHolder& s, double t) override;

	// --------------------------

	/** Jacobian w.r.t. ee position */
	Jac GetJacDynamicsWrtEEPos(const Jac& jac_ee_pos, const Jac& jac_ee_vel,
			const Jac& jac_ee_acc) const override;

	/** How the joint torques affect the dynamic violation */
	Jac GetJacDynamicsWrtJointTorque(const Jac& jac_joint_torque) const
			override;

	/**
	 * @brief How the endeffector forces affect the dynamic violation.
	 */
	Jac GetJacDynamicsWrtForce(const Jac& jac_force) const override;

	/**
	 * @brief How the joint motions affect the dynamic violation.
	 */
	Jac GetJacDynamicsWrtJointMotion(const Jac& jac_joint_pos,
			const Jac& jac_joint_vel, const Jac& jac_joint_acc) const override;

	// --------------------------

	/** Get kinematics violation */
	VectorXd GetKinematicViolation() const;

	/**
	 * @brief How base position affect kinematics
	 */
	Jac GetJacKinematicsWrtBaseLin(const Jac& jac_base_lin_pos) const override;

	/**
	 * @brief How end-effector position affect kinematics.
	 */
	Jac GetJacKinematicsWrtEEPos(const Jac& jac_ee_pos) const override;

	/**
	 * @brief How the joint motions affect the kinematics.
	 */
	Jac GetJacKinematicsWrtJointMotion(const Jac& jac_joint_pos) const override;

	/**
	 * @brief How the base angle affect the kinematics
	 */
	Jac GetJacKinematicsWrtBaseAng(const Jac& jac_base_ang_pos) const override;

	// --------------------------

private:

	Vector3d p_calcualted_; ///< Current base pos in MuJoCo
	Jac p_diff_q_;		 	///< Jacobian of base pos to q
	int mj_id_base_; 		///< Body id of base
};

} /* namespace towr */

#endif /* ROBOTS_MONOPEDMUJOCOMODEL_H_ */
