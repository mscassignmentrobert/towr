/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * dynamics_F_types.h
 *
 * Code generation for function 'dynamics_F_types'
 *
 */

#ifndef DYNAMICS_F_TYPES_H
#define DYNAMICS_F_TYPES_H

/* Include files */
#include "../autogen/rtwtypes.h"
#endif

/* End of code generation (dynamics_F_types.h) */
