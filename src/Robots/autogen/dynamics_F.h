/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * dynamics_F.h
 *
 * Code generation for function 'dynamics_F'
 *
 */

#ifndef DYNAMICS_F_H
#define DYNAMICS_F_H

/* Include files */
#include <cstddef>
#include <cstdlib>

#include "../autogen/dynamics_F_types.h"
#include "../autogen/rtwtypes.h"

/* Function Declarations */
namespace monoped
{
  extern void dynamics_F(const double in1[5], const double in2[5], const double
    in3[4], double F[5]);
  extern void dynamics_F_diff_dq(const double in1[5], const double in2[5], const
    double in3[4], double F_diff_dq[25]);
  extern void dynamics_F_diff_q(const double in1[5], const double in2[5], const
    double in3[4], double F_diff_q[25]);
  extern void dynamics_F_diff_u(const double in1[5], const double in2[5], const
    double in3[4], double F_diff_u[20]);
  extern void dynamics_F_initialize();
  extern void dynamics_F_terminate();
  extern void dynamics_M(const double in1[5], double M[25]);
  extern void dynamics_Mq_diff_q(const double in1[5], const double in2[5], const
    double in3[5], const double in4[4], double Mq_diff_q[25]);
  extern void kin_forward(const double in1[5], double f_for[5]);
  extern void kin_inverse(const double in1[5], double f_inv[5]);
  extern void kin_jacobian_forward(const double in1[5], double J_for[25]);
  extern void kin_jacobian_forward_dt(const double in1[5], const double in2[5],
    double J_for_dt[25]);
}

#endif

/* End of code generation (dynamics_F.h) */
