#ifndef VARIABLES_MYNODESVARIABLESALL_H_
#define VARIABLES_MYNODESVARIABLESALL_H_

#include <vector>
#include <towr/variables/nodes_variables_all.h>

namespace towr
{

class MyNodesVariablesAll: public NodesVariablesAll
{
public:
	MyNodesVariablesAll(int n_nodes, int n_dim, std::string variable_id);
	virtual ~MyNodesVariablesAll() = default;

	/** Add bounds to all nodes in spline */
	void AddGlobalBounds(Dx deriv, const std::vector<int>& dimensions,
			const VectorXd& lower, const VectorXd& upper);

	/** Set current values by interpolating time array */
	void SetByTimeInterpolation(const std::vector<std::pair<double, VectorXd>>& points);

	static VectorXd InterpolatePair(double t, const std::vector<std::pair<double, VectorXd>>& points);
};

} /* namespace towr */

#endif /* VARIABLES_MYNODESVARIABLESALL_H_ */
