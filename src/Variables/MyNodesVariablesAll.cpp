#include "MyNodesVariablesAll.h"

#include <iostream>

namespace towr
{

/**
 * Constructor
 */
MyNodesVariablesAll::MyNodesVariablesAll(int n_nodes, int n_dim,
		std::string variable_id) :
		NodesVariablesAll(n_nodes, n_dim, variable_id)
{

}

/**
 * Add bounds to all nodes of spline
 *
 * @param deriv			To which derivative the bound should apply
 * @param dimensions	To which dimenions of this spline collection it should be applied
 * @param lower			Lower bounds
 * @param upper			Upper bounds
 */
void MyNodesVariablesAll::AddGlobalBounds(Dx deriv,
		const std::vector<int>& dimensions, const VectorXd& lower,
		const VectorXd& upper)
{
	for (auto dim : dimensions)
	{
		for (unsigned int node_id = 0; node_id < nodes_.size(); node_id++)
		{
			int idx = GetOptIndex(NodeValueInfo(node_id, deriv, dim));
			bounds_.at(idx) = ifopt::Bounds(lower[dim], upper[dim]);
		}
	}
}

/**
 * Set current value by interpolating between given points at given times
 *
 * The points should start at t = 0. The last time will be used to scale.
 *
 * @param points	Vector of pairs of <time, value>
 */
void MyNodesVariablesAll::SetByTimeInterpolation(
		const std::vector<std::pair<double, VectorXd>>& points)
{
	double t_total = points.back().first;

	int num_nodes = nodes_.size();

	for (int idx = 0; idx < GetRows(); idx++)
	{
		for (auto nvi : GetNodeValuesInfo(idx))
		{
			double t = static_cast<double>(nvi.id_)
					/ static_cast<double>(num_nodes - 1) * t_total;

			if (nvi.deriv_ == kPos)
			{
				VectorXd pos = InterpolatePair(t, points);
				nodes_.at(nvi.id_).at(kPos)(nvi.dim_) = pos(nvi.dim_);
			}

			if (nvi.deriv_ == kVel)
			{
				// Perform little finite difference to find velocity value at node
				const double eps = 1e-5;
				VectorXd d_pos = InterpolatePair(t + eps, points)
						- InterpolatePair(t - eps, points);
				VectorXd velocity = d_pos / (2.0 * eps);
				nodes_.at(nvi.id_).at(kVel)(nvi.dim_) = velocity(nvi.dim_);
			}
		}
	}
}

/**
 * Interpolate in pair array
 */
MyNodesVariablesAll::VectorXd MyNodesVariablesAll::InterpolatePair(double t,
		const std::vector<std::pair<double, VectorXd>>& points)
{
	// Return first value if too low
	if (t < points.front().first)
	{
		return points.front().second;
	}

	for (unsigned int i = 1; i < points.size(); i++)
	{
		// Find two adjecent points
		if (t >= points[i - 1].first && t < points[i].first)
		{
			double factor = (t - points[i - 1].first)
					/ (points[i].first - points[i - 1].first);

			// Return interpolated value
			return points[i - 1].second
					+ factor * (points[i].second - points[i - 1].second);
		}
	}

	// Return last value of too high
	return points.back().second;
}

} /* namespace towr */
