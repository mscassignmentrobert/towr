#ifndef MY_VARIABLE_NAMES_H_
#define MY_VARIABLE_NAMES_H_

#include <string>
#include <towr/variables/variable_names.h>

namespace towr
{
/**
 * @brief Identifiers (names) used for variables in the optimization problem.
 *
 * @ingroup Variables
 */
namespace id
{

static const std::string joint_torque_nodes = "joint-torque";
static const std::string joint_motion_nodes = "joint-motion";

} // namespace id
} // namespace towr

#endif /* MY_VARIABLE_NAMES_H_ */
