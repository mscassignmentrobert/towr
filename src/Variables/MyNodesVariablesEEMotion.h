#ifndef VARIABLES_MYNODESVARIABLESEEMOTION_H_
#define VARIABLES_MYNODESVARIABLESEEMOTION_H_

#include <towr/variables/nodes_variables_phase_based.h>

namespace towr
{

class MyNodesVariablesEEMotion: public NodesVariablesEEMotion
{
public:
	MyNodesVariablesEEMotion(int phase_count, bool is_in_contact_at_start,
			const std::string& name, int n_polys_in_changing_phase);
	virtual ~MyNodesVariablesEEMotion() = default;

	/** Set current values by interpolating time array */
	void SetByTimeInterpolation(
			const std::vector<std::pair<double, VectorXd>>& points);
};

} /* namespace towr */

#endif /* VARIABLES_MYNODESVARIABLESEEMOTION_H_ */
