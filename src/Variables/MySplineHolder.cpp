#include "../Variables/MySplineHolder.h"

#include <towr/variables/phase_spline.h>

namespace towr
{

/**
 * Pass null pointers to the base motions
 */
MySplineHolder::MySplineHolder(NodesVariables::Ptr joint_motion_nodes,
		const std::vector<double>& joint_poly_durations,
		NodesVariables::Ptr joint_torque_nodes,
		const std::vector<double>& torque_poly_durations,
		std::vector<NodesVariablesPhaseBased::Ptr> ee_motion_nodes,
		std::vector<NodesVariablesPhaseBased::Ptr> ee_force_nodes,
		std::vector<PhaseDurations::Ptr> phase_durations,
		bool ee_durations_change)
{
	phase_durations_ = phase_durations;

	for (uint ee = 0; ee < ee_motion_nodes.size(); ++ee)
	{
		if (ee_durations_change)
		{
			// spline that changes the polynomial durations (affects Jacobian)
			ee_motion_.push_back(
					std::make_shared<PhaseSpline>(ee_motion_nodes.at(ee),
							phase_durations.at(ee).get()));
			ee_force_.push_back(
					std::make_shared<PhaseSpline>(ee_force_nodes.at(ee),
							phase_durations.at(ee).get()));
		}
		else
		{
			// spline without changing the polynomial durations
			auto ee_motion_poly_durations =
					ee_motion_nodes.at(ee)->ConvertPhaseToPolyDurations(
							phase_durations.at(ee)->GetPhaseDurations());
			auto ee_force_poly_durations =
					ee_force_nodes.at(ee)->ConvertPhaseToPolyDurations(
							phase_durations.at(ee)->GetPhaseDurations());

			ee_motion_.push_back(
					std::make_shared<NodeSpline>(ee_motion_nodes.at(ee).get(),
							ee_motion_poly_durations));
			ee_force_.push_back(
					std::make_shared<NodeSpline>(ee_force_nodes.at(ee).get(),
							ee_force_poly_durations));
		}
	}

	// Initialise joint motion nodes spline
	joint_motion_ = std::make_shared<NodeSpline>(joint_motion_nodes.get(),
			joint_poly_durations);

	// Initialise joint torque nodes spline
	joint_torque_ = std::make_shared<NodeSpline>(joint_torque_nodes.get(),
			torque_poly_durations);
}

}
/* namespace towr */
