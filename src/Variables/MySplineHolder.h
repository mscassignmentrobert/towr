#ifndef MYSPLINEHOLDER_H_
#define MYSPLINEHOLDER_H_

#include <towr/variables/spline_holder.h>
#include <towr/variables/phase_durations.h>
#include <towr/variables/node_spline.h>
#include <towr/variables/nodes_variables.h>
#include <towr/variables/nodes_variables_phase_based.h>

namespace towr
{

/**
 * @brief Builds splines from node values (pos/vel) and durations.
 *
 * These splines are linked to the optimization variables, so change as the
 * nodes or durations change. This is a convenience class to not have
 * to construct the splines from the variables new every time.
 *
 * This is an extended version of the default tower one, which also includes
 * joint torques. The base motion is also no longer used. However, we still
 * need to extend the original class to compatible with existing functions.
 */
struct MySplineHolder: public SplineHolder
{
	/**
	 * Complete constructor
	 *
	 * @param joint_motion  The nodes describing the joint positions
	 * @param joint_poly_durations	Durations of each joint polynomial
	 * @param joint_torque  The nodes describing the joint torques
	 * @param torque_poly_durations	Durations of each torque polynomial
	 * @param ee_motion The nodes describing the endeffector motions.
	 * @param ee_force  The nodes describing the endeffector forces.
	 * @param phase_durations  The phase durations of each endeffector.
	 * @param ee_durations_change  True if the ee durations are optimized over.
	 */
	MySplineHolder(NodesVariables::Ptr joint_motion,
			const std::vector<double>& joint_poly_durations,
			NodesVariables::Ptr joint_torque,
			const std::vector<double>& torque_poly_durations,
			std::vector<NodesVariablesPhaseBased::Ptr> ee_motion,
			std::vector<NodesVariablesPhaseBased::Ptr> ee_force,
			std::vector<PhaseDurations::Ptr> phase_durations,
			bool ee_durations_change);

	/// Second empty constructor
	MySplineHolder() = default;

	/// Spline for joint motion
	NodeSpline::Ptr joint_motion_;

	/// Spline for joint torques
	NodeSpline::Ptr joint_torque_;
};

} /* namespace towr */

#endif /* MYSPLINEHOLDER_H_ */
