#include "MyNodesVariablesEEMotion.h"
#include "MyNodesVariablesAll.h"

namespace towr
{

MyNodesVariablesEEMotion::MyNodesVariablesEEMotion(int phase_count,
		bool is_in_contact_at_start, const std::string& name,
		int n_polys_in_changing_phase) :
		NodesVariablesEEMotion(phase_count, is_in_contact_at_start, name,
				n_polys_in_changing_phase)

{

}

/**
 * Set current value by interpolating between given points at given times
 *
 * The points should start at t = 0. The last time will be used to scale.
 *
 * @param points	Vector of pairs of <time, value>
 */
void MyNodesVariablesEEMotion::SetByTimeInterpolation(
		const std::vector<std::pair<double, VectorXd>>& points)
{
	double t_total = points.back().first;

	int num_nodes = nodes_.size();

	for (int idx = 0; idx < GetRows(); idx++)
	{
		for (auto nvi : GetNodeValuesInfo(idx))
		{
			double t = static_cast<double>(nvi.id_)
					/ static_cast<double>(num_nodes - 1) * t_total;

			if (nvi.deriv_ == kPos)
			{
				VectorXd pos = MyNodesVariablesAll::InterpolatePair(t, points);
				nodes_.at(nvi.id_).at(kPos)(nvi.dim_) = pos(nvi.dim_);
			}

			if (nvi.deriv_ == kVel)
			{
				// Perform little finite difference to find velocity value at node
				const double eps = 1e-5;
				VectorXd d_pos = MyNodesVariablesAll::InterpolatePair(t + eps,
						points)
						- MyNodesVariablesAll::InterpolatePair(t - eps, points);
				VectorXd velocity = d_pos / (2.0 * eps);
				nodes_.at(nvi.id_).at(kVel)(nvi.dim_) = velocity(nvi.dim_);
			}
		}
	}
}

} /* namespace towr */
